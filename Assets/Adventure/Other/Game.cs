using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour
{	
	#region Singleton
	
	
	static Game instance;
	
	public static Game Instance
	{
		get { return instance; }
	}
	
	
	#endregion
	
	
	#region Inspector variables
	
	
	public GUISkin CustomSkin;
	public string MainButtonName;
	public string MainMenuSceneName;
	
	
	#endregion
	
	
	#region Fields
	
	
	DialogueManager dialogueManager;
	public DialogueManager DialogueManager
	{
		get { return dialogueManager; }
	}
	
	
	#endregion
	
	
	#region Unity event handlers
	
	
	void Start()
	{
		// Already instantiated
		if (instance != null)
		{
			Destroy(gameObject);
			return;
		}
		
		DontDestroyOnLoad(this);
		instance = this;
		
        dialogueManager = GetComponent<DialogueManager>();
	}
	
	/// <remarks>
	/// For some reason this gets called twice whenever the
	/// first/original scene is loaded.
	/// </remarks>
	void OnLevelWasLoaded(int level)
	{	
		string currentScene = Application.loadedLevelName;
		
		// make all objects added to any scene while playing inactive
		foreach (SceneGO r in addedToScene)
		{
			// assume if not active, children won't be as well
			if (r.go.active)
			{
				r.go.active = false;
				
				// include all children
				foreach (Transform t in r.go.transform)
					t.gameObject.active = false;
			}
		}
		
		// remove all objects that were set to be removed in this scene
		List<SceneGOName> removedFromThisScene = 
			removedFromScene.FindAll(r => (r.sceneId == currentScene));
		foreach (SceneGOName r in removedFromThisScene)
		{
			GameObject[] toRemove = Helpers.GetAllGameObjects(r.goName);
			
			if ((toRemove == null) || (toRemove.Length <= 0))
				continue;
			
			foreach (GameObject go in toRemove)
			{
				if (go.active)
					DestroyImmediate(go);
			}
		}
		
		// make all objects added to this scene while playing active
		foreach (SceneGO r in addedToScene)
		{
			if (r.sceneId == currentScene)
			{
				r.go.active = true;
				
				// include all children
				foreach (Transform t in r.go.transform)
					t.gameObject.active = true;
			}
		}
		
		Helpers.UpdateNavMesh();
	}
	
	
	#endregion
	
	
	#region Public methods
	
	
	/// <summary>
	/// Removes all game objects with "go.name" from current scene
	/// every time it's loaded from now on.
	/// </summary>
	/// <param name="removeThisObject">
	/// Whether "go" should be removed from current scene (if already added) or
	/// added to current scene (if not already added).
	/// </param>
	/// <param name="removeOtherObjects">
	/// Whether other objects with "go.name" should be removed from current scene
	/// (if already added) or added to current scene (if not already added).
	/// </param>
	/// <remarks>
	/// When searching for other objects of same name in current scene,
	/// only active objects can be found with this method (Unity's fault).
	/// </remarks>
	public void RemoveObjectsWithNameFromScene(GameObject go, 
		bool removeThisObject = false, bool removeOtherObjects = false)
	{
		if (go == null)
			return;
		
		string currentScene = Application.loadedLevelName;
		
		RemoveGOName(go.name, currentScene);
		
		SetGOInScene(go, currentScene, removeThisObject);
		
		GameObject[] objectsWithName = Helpers.GetAllGameObjects(go.name);
		
		foreach (GameObject o in objectsWithName)
		{
			// this object / 'go', already handled
			if (o == go)
			{
				continue;
			}
			// another object with 'go.name'
			else
			{
				SetGOInScene(o, currentScene, removeOtherObjects);
			}
		}
	}
	
	/// <summary>
	/// Makes sure a game object is added to current scene
	/// every time it's loaded from now on.
	/// </summary>
	public void AddObjectToScene(GameObject go)
	{
		if (go == null)
			return;
		
		AddGOToScene(go, Application.loadedLevelName);
	}
	
	
	/// <summary>
	/// Change to main menu and erases all player progress.
	/// </summary>
	public void ToMainMenu()
	{
		Application.LoadLevel(MainMenuSceneName);
		Destroy(Player.Instance);
		Destroy(this);
	}
	
	
	#endregion
	
	
	#region Scene management
	
	
	/// <remarks>
	/// Removes based on name, make sure all object set to delete
	/// have no other objects in same scene with same name.
	/// </remarks>
	public struct SceneGOName
	{
		public string sceneId;
		public string goName;
		
		public SceneGOName(string sceneId, string goName)
		{
			this.sceneId = sceneId;
			this.goName = goName;
		}
		
      	public static bool operator== (
			SceneGOName r1, SceneGOName r2)
        {
            return ((r1.sceneId == r2.sceneId) &&
				(r1.goName == r2.goName));
        }
		
      	public static bool operator!= (
			SceneGOName r1, SceneGOName r2)
        {
            return ((r1.sceneId != r2.sceneId) ||
				(r1.goName != r2.goName));
        }
		
		public override int GetHashCode()
        {
            return sceneId.GetHashCode() ^ goName.GetHashCode();
        }
		
		public override bool Equals(object o)
		{
			if (o.GetType() != this.GetType())
				return false;
			
			SceneGOName other = (SceneGOName)o;
			
			if ((this.sceneId == other.sceneId) && (this.goName == other.goName))
				return true;
			
			return false;
		}
	}
	
	/// <remarks>
	/// All added object references are stored in a list (i.e., memory),
	/// make sure not too many objects end up in this list.
	/// </remarks>
	public struct SceneGO
	{
		public string sceneId;
		public GameObject go;
		
		public SceneGO(string sceneId, GameObject go)
		{
			this.sceneId = sceneId;
			this.go = go;
		}
		
      	public static bool operator== (
			SceneGO r1, SceneGO r2)
        {
            return ((r1.sceneId == r2.sceneId) &&
				(r1.go == r2.go));
        }
		
      	public static bool operator!= (
			SceneGO r1, SceneGO r2)
        {
            return ((r1.sceneId != r2.sceneId) ||
				(r1.go != r2.go));
        }
		
		public override int GetHashCode()
        {
            return sceneId.GetHashCode() ^ go.GetHashCode();
        }
		
		public override bool Equals(object o)
		{
			if (o.GetType() != this.GetType())
				return false;
			
			SceneGO other = (SceneGO)o;
			
			if ((this.sceneId == other.sceneId) && (this.go == other.go))
				return true;
			
			return false;
		}
	}
	
	/// <summary>
	/// Name of all game objects that should be removed from each scene.
	/// </summary>
	List<SceneGOName> removedFromScene = 
		new List<SceneGOName>();
	
	public SceneGOName[] RemovedFromScene
	{
		get { return removedFromScene.ToArray(); }
	}
	
	#if UNITY_EDITOR
	[SerializeField,HideInInspector]
	public bool foldoutRemovedSection = true;
	#endif
		
	/// <summary>
	/// Name of all game objects that should be removed from each scene.
	/// </summary>
	List<SceneGO> addedToScene = 
		new List<SceneGO>();
	
	public SceneGO[] AddedToScene
	{
		get { return addedToScene.ToArray(); }
	}
	
	#if UNITY_EDITOR
	[SerializeField,HideInInspector]
	public bool foldoutAddedSection = true;
	#endif
	
	/// <summary>
	/// Removes all GO's with "goName" from scene with "sceneId".
	/// </summary>
	/// <param name='goName'>
	/// Go name.
	/// </param>
	/// <param name='sceneId'>
	/// Scene identifier.
	/// </param>
	void RemoveGOName(string goName, string sceneId)
	{
		SceneGOName alreadyRemoved = 
			removedFromScene.Find(r => (r.goName == goName) && 
				(r.sceneId == sceneId));
		
		// 'go.name' hasn't been removed from this scene before
		if (alreadyRemoved == default(SceneGOName))
		{
			removedFromScene.Add(new SceneGOName(sceneId, goName));
			
			Debug.Log("Set object(s) with name (" + goName +
				") to be removed from scene (" + sceneId + ")");
		}
	}
	
	/// <summary>
	/// Either remove "go" from added list for scene with "sceneId" or
	/// add "go" to scene with "sceneId" if "!removeObject".
	/// </summary>
	void SetGOInScene(GameObject go, string sceneId, bool removeObject)
	{
		if (go == null)
			return;
		
		List<SceneGO> alreadyAdded = 
			addedToScene.FindAll(r => (r.go == go) && (r.sceneId == sceneId));
		
		// "go" has not yet been added to current scene
		if ((alreadyAdded == null) || (alreadyAdded.Count <= 0))
		{
			// add object to current scene
			if (!removeObject)
			{
				AddGOToScene(go, sceneId);
			}
		}
		// "go" has already been added to current scene
		else
		{
			foreach (SceneGO added in alreadyAdded)
			{
				// remove object from added list if been added while playing
				if (removeObject)
				{
					addedToScene.Remove(added);
				}
			}
		}
	}
	
	/// <summary>
	/// Adds "go" to scene with "sceneId", removing it from other scenes if necessary.
	/// </summary>
	void AddGOToScene(GameObject go, string sceneId)
	{
		if (go == null)
			return;
		
		// remove object from other scenes first
		addedToScene.RemoveAll(added => added.go == go);
	
		addedToScene.Add(new SceneGO(sceneId, go));
		
		// parent it to this GO so it won't be destroyed on scene change
		go.transform.parent = gameObject.transform;
			
		Debug.Log("Added object (" + go.name +
			") to scene (" + sceneId + ")");
	}
	
	
	#endregion
	
	
	#region Flags
	
	
	List<string> flags = new List<string>();
	
	/// <summary>
	/// Gets the list of global, named flags.
	/// </summary>
	public string[] Flags
	{
		get { return flags.ToArray(); }
	}
	
	/// <summary>
	/// Sets the value of a global, named flag.
	/// </summary>
	public void SetFlag(string name, bool val)
	{		
		string flag = flags.Find(f => f == name);
		
		// not in list 
		if (flag == default(string))
		{
			// add / set to true
			if (val)
				flags.Add(name);
		}
		else
		{
			// remove / set to false
			if (!val)
				flags.Remove(name);
		}
	}
	
	/// <summary>
	/// Gets the value of a global, named flag.
	/// </summary>
	public bool GetFlag(string name)
	{
		string flag = flags.Find(f => f == name);
		
		// not in list 
		if (flag == default(string))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	#if UNITY_EDITOR
	[SerializeField,HideInInspector]
	public bool foldoutFlagsSection = true;
	#endif
	
	
	#endregion
}
